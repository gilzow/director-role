<?php
/**
 * Plugin Name: Director Role plugin
 * Plugin URI: http://universityaffairs.missouri.edu/department/web-communications/
 * Description: Temporary plugin to grant ability to edit theme settings
 * Version: v0.1.4
 * Author: Paul F. Gilzow, Web Communications, University of Missouri
 * Author URI: http://universityaffairs.missouri.edu/department/web-communications/
 * @author Paul F. Gilzow, Web Communications, University of Missouri
 * @copyright 2015 Curators of the University of Missouri
 * @version 0.1.4
 */

class Director {
    public function __construct()
    {
        add_filter('editable_roles',array(&$this,'removeDirector'));
        add_filter('map_meta_cap',array(&$this,'mapCapabilities'),10,4);
    }

    /**
     * If the current user is not an administrator, remove the administrator role from the roles list
     *
     * If they are not a manager, then they shouldnt be able to add a new user as an administrator
     *
     * @todo should they be able to add another manager?
     *
     * @param $aryRoles
     * @return $aryRoles
     */
    public function removeDirector($aryRoles)
    {
        if(isset($aryRoles['director']) && !current_user_can('administrator')){
            unset($aryRoles['director']);
            //unset($aryRoles['manager']);
        }

        return $aryRoles;
    }

    public function mapCapabilities($aryCapabilities,$strCurrentCapability,$intUserId,$aryArgs)
    {
        switch($strCurrentCapability){
            case "edit_user":
                //pass through done intentionally
            case "remove_user":
                //pass through done intentionally
            case "promote_user":
                if(!isset($aryArgs[0]) || (isset($aryArgs[0]) && $aryArgs[0] != $intUserId)){
                    if(isset($aryArgs[0])){
                        if(!$this->_canTheyEditThisPerson($aryArgs[0])){
                            $aryCapabilities[] = 'do_not_allow';
                        }
                    } else {
                        $aryCapabilities[] = 'do_not_allow';
                    }
                }
                break;
            case 'delete_user':
                //pass through done intentionally
            case 'delete_users':
                if(isset($aryArgs[0])){
                    if(!$this->_canTheyEditThisPerson($aryArgs[0])){
                        $aryCapabilities[] = 'do_now_allow';
                    }
                }
        }

        return $aryCapabilities;
    }

    protected function _canTheyEditThisPerson($intID)
    {
        $boolReturn = true;
        $objUserToEdit = new WP_User(absint($intID));
        if(($objUserToEdit->has_cap('administrator') || $objUserToEdit->has_cap('director')) && !current_user_can('administrator')){
            $boolReturn = false;
        }

        return $boolReturn;
    }
}

function mizzouDirectorSetUp()
{
    new Director();
}
add_action('init','mizzouDirectorSetUp');

/**
 * Creates a new role called Manager that has all the capabilities of the Editor role, plus
 * the ability to add edit delete users.
 */
function mizzouAddDirectorRole()
{
    if(!is_null(get_role('director'))){
        remove_role('director');
    }

    //we dont want to add the director role if it already exists
    if(is_null(get_role('director'))){
        $aryNewCaps = array(
            'edit_theme_options',
        );

        //get the editor role so we can clone it
        $objManagerRole = get_role('manager');
        //create our new role with the same caps as editor

        if(!is_null($objManagerRole) && $objManagerRole instanceof WP_Role){
            $objDirectorRole = add_role('director','Director',$objManagerRole->capabilities);
            //now let's add on our extra caps
            foreach($aryNewCaps as $strNewCap){
                $objDirectorRole->add_cap($strNewCap);
            }
        } else {
            return new WP_Error('no_manager','Manager role is required before I can create a director');
        }
    }
}

register_activation_hook(__FILE__,'mizzouAddDirectorRole');

/**
 * plugin update check
 */
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'plugin-update-checker.php';
new PluginUpdateChecker_2_0('https://kernl.us/api/v1/updates/5662023804c34abe792efe10',__FILE__,'director-role',1);